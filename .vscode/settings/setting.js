
function convertArrayOfObjectsToCSV(args) {
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
}

function downloadCSV(args) {
    var data, filename, link;
    var csv = convertArrayOfObjectsToCSV({
        data: stockData
    });
    if (csv == null) return;

    filename = args.filename || 'setting.csv';

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();
}

const https = require('https');

https.get('https://fleet2.bonzaicloud.com/hail/00b8b95d-a64d-4054-bf82-32420677e6ef', (resp) => {
let data = '';

// A chunk of data has been recieved.
resp.on('data', (chunk) => {
data += chunk;
});

// The whole response has been received. Print out the result.
resp.on('end', () => {
console.log(JSON.parse(data));
});

}).on("error", (err) => {
console.log("Error: " + err.message);
var csv = convertArrayOfObjectsToCSV([JSON.parse(data)])
console.log(csv);
});